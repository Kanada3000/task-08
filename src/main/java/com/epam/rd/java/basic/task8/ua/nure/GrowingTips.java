package com.epam.rd.java.basic.task8.ua.nure;

import java.math.BigInteger;

//@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(name = "", propOrder = {
//        "tempreture",
//        "lighting",
//        "watering"
//})
public class GrowingTips {

//    @XmlElement(required = true)
    protected Tempreture tempreture;
//    @XmlElement(required = true)
    protected Lighting lighting;
    protected Watering watering;

    public Tempreture getTempreture() {
        return tempreture;
    }

    public void setTempreture(Tempreture value) {
        this.tempreture = value;
    }

    public Lighting getLighting() {
        return lighting;
    }

    public void setLighting(Lighting value) {
        this.lighting = value;
    }

    public Watering getWatering() {
        return watering;
    }

    public void setWatering(Watering value) {
        this.watering = value;
    }




}
