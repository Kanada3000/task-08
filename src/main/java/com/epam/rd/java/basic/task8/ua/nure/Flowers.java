package com.epam.rd.java.basic.task8.ua.nure;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlAttribute;
//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlRootElement;
//import javax.xml.bind.annotation.XmlSchemaType;
//import javax.xml.bind.annotation.XmlType;
//import javax.xml.bind.annotation.XmlValue;


import java.util.ArrayList;
import java.util.List;

//@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(name = "", propOrder = {
//    "flower"
//})
//@XmlRootElement(name = "flowers")
public class Flowers {

    protected List<Flower> flower;

    public List<Flower> getFlower() {
        if (flower == null) {
            flower = new ArrayList<Flower>();
        }
        return this.flower;
    }



}
