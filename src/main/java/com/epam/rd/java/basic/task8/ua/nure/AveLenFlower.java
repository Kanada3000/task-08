package com.epam.rd.java.basic.task8.ua.nure;

import java.math.BigInteger;

public class AveLenFlower {

    //        @XmlValue
//        @XmlSchemaType(name = "positiveInteger")
    protected BigInteger value;
    //        @XmlAttribute(name = "measure", required = true)
    protected String measure;


    public BigInteger getValue() {
        return value;
    }


    public void setValue(BigInteger value) {
        this.value = value;
    }

    public String getMeasure() {
        if (measure == null) {
            return "cm";
        } else {
            return measure;
        }
    }

    public void setMeasure(String value) {
        this.measure = value;
    }

}