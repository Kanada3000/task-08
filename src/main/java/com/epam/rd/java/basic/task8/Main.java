package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.ua.nure.Flower;
import com.epam.rd.java.basic.task8.ua.nure.Flowers;
import com.epam.rd.java.basic.task8.util.Sorter;
import org.xml.sax.SAXException;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }

        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        // get container
        DOMController domController = new DOMController(xmlFileName);
        try {
            domController.parse(true);
        } catch (SAXException ex) {
            ex.printStackTrace();
            System.err.println("====================================");
            System.err.println("XML not valid");
            System.err.println("Test object --> " + domController.getFlowers());
            System.err.println("====================================");
        }

        Flowers flowers = domController.getFlowers();

        // sort (case 1)
        Sorter.sortFlowersByFlowerName(flowers);

        // save
        String outputXmlFile = "output.dom.xml";
        DOMController.saveXML(flowers, outputXmlFile);

//		////////////////////////////////////////////////////////
//		// SAX
//		////////////////////////////////////////////////////////
//
//		// get
        SAXController saxController = new SAXController(xmlFileName);
        saxController.parse(true);
        flowers = saxController.getFlowers();
//
//		// sort  (case 2)
        Sorter.sortFlowersByFlowerOrigin(flowers);
//
//		// save
        outputXmlFile = "output.sax.xml";
        DOMController.saveXML(flowers, outputXmlFile);
//
//		////////////////////////////////////////////////////////
//		// StAX
//		////////////////////////////////////////////////////////
//
//		// get
        STAXController staxController = new STAXController(xmlFileName);
        staxController.parse();
        flowers = staxController.getFlowers();
//
//		// sort  (case 3)
        Sorter.sortFlowersByFlowerName(flowers);
//
//		// save
        outputXmlFile = "output.stax.xml";
        DOMController.saveXML(flowers, outputXmlFile);
    }

}
