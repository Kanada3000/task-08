package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.Constants;
import com.epam.rd.java.basic.task8.constants.XML;
import com.epam.rd.java.basic.task8.ua.nure.*;
import com.epam.rd.java.basic.task8.util.XSDValidator;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.math.BigInteger;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private String xmlFileName;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public void parse(boolean validate) throws ParserConfigurationException,
            SAXException, IOException {

        // get sax parser factory
        // this way you obtain SAX parser factory based on internal implementation
        // of the XERCES library bundled with jdk
        //
        // if you place xercesImpl.jar to application classpath the following invocation:
        // 		SAXParserFactory.newInstance()
        // returns factory based on the external XERCES library
        // (see xercesImpl.jar/META-INF/services/javax.xml.parsers.SAXParserFactory)
        //
        // If there is no xercesImpl.jar in classpath then
        // internal implementation of XERCES will be used automatically
        // i.e. in this case you may use the following code:
        // 		SAXParserFactory factory = SAXParserFactory.newInstance();

        SAXParserFactory factory = SAXParserFactory.newInstance(
                Constants.CLASS__SAX_PARSER_FACTORY_INTERNAL,
                this.getClass().getClassLoader());


        factory.setNamespaceAware(true);
        if (validate) {
            XSDValidator validator = new XSDValidator("input.xsd");
            validator.validate(xmlFileName);
        }

        SAXParser parser = factory.newSAXParser();
        parser.parse(xmlFileName, this);
    }

    // ///////////////////////////////////////////////////////////
    // ERROR HANDLER IMPLEMENTATION
    // ///////////////////////////////////////////////////////////

    @Override
    public void error(org.xml.sax.SAXParseException e) throws SAXException {
        throw e; // <-- if XML document not valid just throw exception
    }

    ;

    // ///////////////////////////////////////////////////////////
    // CONTENT HANDLER IMPLEMENTATION
    // ///////////////////////////////////////////////////////////

    private String currentElement; // <-- current element name holder

    private Flowers flowers; // <-- main container
    private Flower flower;
    private AveLenFlower aveLenFlower;
    private GrowingTips growingTips;
    private Lighting lighting;
    private Tempreture tempreture;
    private VisualParameters visualParameters;
    private Watering watering;

    public Flowers getFlowers() {
        return flowers;
    }

    @Override
    public void startElement(String uri, String localName, String qName,
                             Attributes attributes) throws SAXException {

        currentElement = localName;

        // WARNING!!!
        // here and below we use '==' operation to compare two INTERNED STRINGS
        if (currentElement == XML.FLOWERS.value()) {
            flowers = new Flowers();
            return;
        }

        if (currentElement == XML.FLOWER.value()) {
            flower = new Flower();
            return;
        }

        if (currentElement == XML.AVE_LEN_FLOWER.value()) {
            aveLenFlower = new AveLenFlower();
            if (attributes.getLength() > 0) {
                aveLenFlower.setMeasure(attributes.getValue(uri, XML.MEASURE.value()));
            }
            return;
        }

        if (currentElement == XML.GROWING_TIPS.value()) {
            growingTips = new GrowingTips();
            return;
        }

        if (currentElement == XML.LIGHTNING.value()) {
            lighting = new Lighting();
            if (attributes.getLength() > 0) {
                lighting.setLightRequiring(attributes.getValue(XML.LIGHT_REQUIRING.value()));
            }
            return;
        }

        if (currentElement == XML.TEMPRETURE.value()) {
            tempreture = new Tempreture();
            if (attributes.getLength() > 0) {
                tempreture.setMeasure(attributes.getValue(uri, XML.MEASURE.value()));
            }
            return;
        }

        if (currentElement == XML.VISUAL_PARAMETERS.value()) {
            visualParameters = new VisualParameters();
            return;
        }

        if (currentElement == XML.WATERING.value()) {
            watering = new Watering();
            if (attributes.getLength() > 0) {
                watering.setMeasure(attributes.getValue(uri, XML.MEASURE.value()));
            }
        }
    }

    @Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {

        String elementText = new String(ch, start, length).trim();

        if (elementText.isEmpty()) // <-- return if content is empty
            return;

        if (currentElement == XML.NAME.value()) {
            flower.setName(elementText);
            return;
        }

        if (currentElement == XML.SOIL.value()) {
            flower.setSoil(elementText);
            return;
        }

        if (currentElement == XML.ORIGIN.value()) {
            flower.setOrigin(elementText);
            return;
        }

        if (currentElement == XML.STEM_COLOUR.value()) {
            visualParameters.setStemColour(elementText);
            return;
        }

        if (currentElement == XML.LEAF_COLOUR.value()) {
            visualParameters.setLeafColour(elementText);
            return;
        }

        if (currentElement == XML.AVE_LEN_FLOWER.value()) {
            aveLenFlower.setValue(BigInteger.valueOf(Long.parseLong(elementText)));
            return;
        }

        if (currentElement == XML.TEMPRETURE.value()) {
            tempreture.setValue(BigInteger.valueOf(Long.parseLong(elementText)));
            return;
        }

        if (currentElement == XML.WATERING.value()) {
            watering.setValue(BigInteger.valueOf(Long.parseLong(elementText)));
            return;
        }

        if (currentElement == XML.MULTIPLYING.value()) {
            flower.setMultiplying(elementText);
        }

    }

    @Override
    public void endElement(String uri, String localName, String qName)
            throws SAXException {

        if (localName == XML.FLOWER.value()) {
            flowers.getFlower().add(flower);
            return;
        }

        if (localName == XML.AVE_LEN_FLOWER.value()) {
            visualParameters.setAveLenFlower(aveLenFlower);
            return;
        }

        if (localName == XML.GROWING_TIPS.value()) {
            flower.setGrowingTips(growingTips);
        }

        if (localName == XML.LIGHTNING.value()) {
            growingTips.setLighting(lighting);
            return;
        }

        if (localName == XML.TEMPRETURE.value()) {
            growingTips.setTempreture(tempreture);
            return;
        }

        if (localName == XML.VISUAL_PARAMETERS.value()) {
            flower.setVisualParameters(visualParameters);
            return;
        }

        if (localName == XML.WATERING.value()) {
            growingTips.setWatering(watering);
        }
    }

}