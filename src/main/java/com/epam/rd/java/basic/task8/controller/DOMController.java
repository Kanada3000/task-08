package com.epam.rd.java.basic.task8.controller;

import java.io.IOException;
import java.math.BigInteger;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import com.epam.rd.java.basic.task8.constants.Constants;
import com.epam.rd.java.basic.task8.constants.XML;
import com.epam.rd.java.basic.task8.ua.nure.*;
import com.epam.rd.java.basic.task8.util.Transformer;
import com.epam.rd.java.basic.task8.util.XSDValidator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	private Flowers flowers;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers getFlowers(){
		return flowers;
	}

	public void parse(boolean validate) throws ParserConfigurationException,
			SAXException, IOException {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(
				Constants.CLASS__DOCUMENT_BUILDER_FACTORY_INTERNAL,
				this.getClass().getClassLoader());

		dbf.setAttribute(javax.xml.XMLConstants.ACCESS_EXTERNAL_DTD, "");
		dbf.setAttribute(javax.xml.XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");


		dbf.setNamespaceAware(true);
		if (validate) {

			XSDValidator validator = new XSDValidator("input.xsd");
			validator.validate(xmlFileName);
		}

		DocumentBuilder db = dbf.newDocumentBuilder();

		db.setErrorHandler(new DefaultHandler() {
			@Override
			public void error(SAXParseException e) throws SAXException {
				throw e;
			}
		});

		Document document = db.parse(xmlFileName);
		Element root = document.getDocumentElement();


		flowers = new Flowers();

		NodeList flowerNodes = root
				.getElementsByTagName(XML.FLOWER.value());

		for (int j = 0; j < flowerNodes.getLength(); j++) {
			Flower flower = getFlower(flowerNodes.item(j));
			flowers.getFlower().add(flower);
		}
	}

	private Flower getFlower(Node fNode) {
		Flower flower = new Flower();
		Element fElement = (Element) fNode;

		Node nNode = fElement.getElementsByTagName(XML.NAME.value())
				.item(0);
		flower.setName(nNode.getTextContent());

		Node sNode = fElement.getElementsByTagName(XML.SOIL.value())
				.item(0);
		flower.setSoil(sNode.getTextContent());

		Node oNode = fElement.getElementsByTagName(XML.ORIGIN.value())
				.item(0);
		flower.setOrigin(oNode.getTextContent());

		Node vpNode = fElement.getElementsByTagName(XML.VISUAL_PARAMETERS.value())
				.item(0);
		VisualParameters vp = getVisualParameters(vpNode);
		flower.setVisualParameters(vp);

		Node gtNode = fElement.getElementsByTagName(XML.GROWING_TIPS.value())
				.item(0);
		GrowingTips gt = getGrowingTips(gtNode);
		flower.setGrowingTips(gt);

		Node mNode = fElement.getElementsByTagName(XML.MULTIPLYING.value())
				.item(0);
		flower.setMultiplying(mNode.getTextContent());

		return flower;
	}

	private GrowingTips getGrowingTips(Node gtNode) {
		GrowingTips growingTips = new GrowingTips();
		Element gtElement = (Element) gtNode;

		Node tNode = gtElement.getElementsByTagName(XML.TEMPRETURE.value())
				.item(0);
		growingTips.setTempreture(getTempreture(tNode));

		Node lNode = gtElement.getElementsByTagName(XML.LIGHTNING.value())
				.item(0);
		growingTips.setLighting(getLighting(lNode));

		NodeList wNodeList = gtElement.getElementsByTagName(XML.WATERING.value());
		if(wNodeList.getLength() > 0){
			Node wNode = wNodeList.item(0);
			growingTips.setWatering(getWatering(wNode));
		}

		return growingTips;
	}

	private Watering getWatering(Node wNode) {
		Watering watering = new Watering();
		Element wElement = (Element) wNode;

		String measure = wElement.getAttribute(XML.MEASURE.value());
		watering.setMeasure(measure);

		String content = wElement.getTextContent();
		watering.setValue(BigInteger.valueOf(Long.parseLong(content)));

		return watering;
	}

	private Lighting getLighting(Node lNode) {
		Lighting lighting = new Lighting();
		Element lElement = (Element) lNode;

		String lightRequiring = lElement.getAttribute(XML.LIGHT_REQUIRING.value());
		lighting.setLightRequiring(lightRequiring);

		return lighting;
	}

	private Tempreture getTempreture(Node tNode) {
		Tempreture tempreture = new Tempreture();
		Element tElement = (Element) tNode;

		String measure = tElement.getAttribute(XML.MEASURE.value());
		tempreture.setMeasure(measure);

		String content = tElement.getTextContent();
		tempreture.setValue(BigInteger.valueOf(Long.parseLong(content)));

		return tempreture;
	}

	private VisualParameters getVisualParameters(Node vpNode) {
		VisualParameters visualParameters = new VisualParameters();
		Element vpElement = (Element) vpNode;

		Node scNode = vpElement.getElementsByTagName(XML.STEM_COLOUR.value())
				.item(0);
		visualParameters.setStemColour(scNode.getTextContent());

		Node lcNode = vpElement.getElementsByTagName(XML.LEAF_COLOUR.value())
				.item(0);
		visualParameters.setLeafColour(lcNode.getTextContent());

		NodeList alfNodeList = vpElement.getElementsByTagName(XML.AVE_LEN_FLOWER.value());
		if(alfNodeList.getLength() > 0){
			Node alfNode = alfNodeList.item(0);
			visualParameters.setAveLenFlower(getAveLenFlower(alfNode));
		}

		return visualParameters;
	}

	private AveLenFlower getAveLenFlower(Node alfNode) {
		AveLenFlower aveLenFlower = new AveLenFlower();
		Element alfElement = (Element) alfNode;

		String measure = alfElement.getAttribute(XML.MEASURE.value());
		aveLenFlower.setMeasure(measure);

		String content = alfElement.getTextContent();
		aveLenFlower.setValue(BigInteger.valueOf(Long.parseLong(content)));

		return aveLenFlower;
	}

	public static void saveXML(Flowers flowers, String xmlFileName)
			throws ParserConfigurationException, TransformerException {

		// obtain DOM parser

		// get document builder factory
		// this way you obtain DOM analyzer based on internal implementaion
		// of the XERCES library bundled with jdk
		//
		// if you place xercesImpl.jar to application classpath the following invocation:
		// 		DocumentBuilderFactory.newInstance()
		// returns factory based on the external XERCES library.
		// (see xercesImpl.jar/META-INF/services/javax.xml.parsers.DocumentBuilderFactory)
		//
		// if there is no xercesImpl.jar in classpath then
		// internal implementation of XERCES will be used automatically
		// i.e. in this case you may use the following code:
		// 		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(Constants.CLASS__DOCUMENT_BUILDER_FACTORY_INTERNAL,
				DOMController.class.getClass().getClassLoader());
		// LET SONAR BE HAPPY
		dbf.setAttribute(javax.xml.XMLConstants.ACCESS_EXTERNAL_DTD, "");
		dbf.setAttribute(javax.xml.XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");

		// set properties for Factory
		dbf.setNamespaceAware(true); // <-- XML document has namespace

		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.newDocument();

		// this is the root element
		Element ffElement = document.createElementNS(
				Constants.MY_NS__URI, XML.FLOWERS.value());

		// set schema location
		ffElement.setAttributeNS(
				XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI,
				Constants.SCHEMA_LOCATION__ATTR_FQN,
				Constants.SCHEMA_LOCATION__URI);

		document.appendChild(ffElement);

		for (Flower flower : flowers.getFlower()) {

			Element fElement = document.createElement(XML.FLOWER.value());
			ffElement.appendChild(fElement);

			{
				Element nElement = document.createElement(XML.NAME.value());
				nElement.setTextContent(flower.getName());
				fElement.appendChild(nElement);

				Element sElement = document.createElement(XML.SOIL.value());
				sElement.setTextContent(flower.getSoil());
				fElement.appendChild(sElement);

				Element oElement = document.createElement(XML.ORIGIN.value());
				oElement.setTextContent(flower.getOrigin());
				fElement.appendChild(oElement);

				{
					Element vpElement = document.createElement(XML.VISUAL_PARAMETERS.value());
					fElement.appendChild(vpElement);

					Element scElement = document.createElement(XML.STEM_COLOUR.value());
					scElement.setTextContent(flower.getVisualParameters().getStemColour());
					vpElement.appendChild(scElement);

					Element lcElement = document.createElement(XML.LEAF_COLOUR.value());
					lcElement.setTextContent(flower.getVisualParameters().getLeafColour());
					vpElement.appendChild(lcElement);

					if (flower.getVisualParameters().getAveLenFlower() != null) {
						Element alfElement = document.createElement(XML.AVE_LEN_FLOWER.value());
						alfElement.setTextContent(flower.getVisualParameters().getAveLenFlower().getValue().toString());
						alfElement.setAttribute(XML.MEASURE.value(),
								flower.getVisualParameters().getAveLenFlower().getMeasure());
						vpElement.appendChild(alfElement);
					}
				}

				{
					Element gtElement = document.createElement(XML.GROWING_TIPS.value());
					fElement.appendChild(gtElement);

					Element tElement = document.createElement(XML.TEMPRETURE.value());
					tElement.setAttribute(XML.MEASURE.value(), flower.getGrowingTips().getTempreture().getMeasure());
					tElement.setTextContent(flower.getGrowingTips().getTempreture().getValue().toString());
					gtElement.appendChild(tElement);

					Element lElement = document.createElement(XML.LIGHTNING.value());
					lElement.setAttribute(XML.LIGHT_REQUIRING.value(),
							flower.getGrowingTips().getLighting().getLightRequiring());
					gtElement.appendChild(lElement);

					if (flower.getVisualParameters().getAveLenFlower() != null) {
						Element wElement = document.createElement(XML.WATERING.value());
						wElement.setAttribute(XML.MEASURE.value(), flower.getGrowingTips().getWatering().getMeasure());
						wElement.setTextContent(flower.getGrowingTips().getWatering().getValue().toString());
						gtElement.appendChild(wElement);
					}
				}
				Element mElement = document.createElement(XML.MULTIPLYING.value());
				mElement.setTextContent(flower.getMultiplying());
				fElement.appendChild(mElement);
			}
		}

		Transformer.saveToXML(document, xmlFileName);
	}


}
