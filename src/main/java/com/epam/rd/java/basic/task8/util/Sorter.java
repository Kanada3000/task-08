package com.epam.rd.java.basic.task8.util;

import com.epam.rd.java.basic.task8.ua.nure.Flower;
import com.epam.rd.java.basic.task8.ua.nure.Flowers;
import com.epam.rd.java.basic.task8.ua.nure.GrowingTips;
import com.epam.rd.java.basic.task8.ua.nure.VisualParameters;

import java.util.Collections;
import java.util.Comparator;

public class Sorter {
	////////////////////////////////////////////////////////////
	// these are comparators
	////////////////////////////////////////////////////////////

	/**
	 * Sorts questions by question text
	 */
	public static final Comparator<Flower> sortFlowersByName = new Comparator<Flower>() {
		@Override
		public int compare(Flower o1, Flower o2) {
			return o1.getName().compareTo(o2.getName());
		}
	};
	public static final Comparator<Flower> sortFlowersByOrigin = new Comparator<Flower>() {
		@Override
		public int compare(Flower o1, Flower o2) {
			return o1.getOrigin().compareTo(o2.getOrigin());
		}
	};

	public static final Comparator<VisualParameters> sortVisualParametersByLeafColour = new Comparator<VisualParameters>() {
		@Override
		public int compare(VisualParameters o1, VisualParameters o2) {
			return o1.getLeafColour().compareTo(o2.getLeafColour());
		}
	};

	/**
	 * Sorts answers by correct.
	 */
	public static final Comparator<GrowingTips> sortGrowingTipsByTempreture = new Comparator<GrowingTips>() {
		@Override
		public int compare(GrowingTips o1, GrowingTips o2) {
            return o1.getTempreture().getValue().compareTo(o2.getTempreture().getValue());
		}
	};

	////////////////////////////////////////////////////////////
	// these methods take Test object and sort it
	// with according comparator
	////////////////////////////////////////////////////////////

	public static final void sortFlowersByFlowerName(Flowers flowers) {
		Collections.sort(flowers.getFlower(),
				sortFlowersByName);
	}

	public static final void sortFlowersByFlowerOrigin(Flowers flowers) {
		Collections.sort(flowers.getFlower(),
				sortFlowersByOrigin);
	}
}