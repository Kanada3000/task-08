package com.epam.rd.java.basic.task8.util;

import org.w3c.dom.Document;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;

public class Transformer {

	public static void saveToHTML(String xmlFileName, String xslFileName,
			String htmlFileName) throws TransformerException {
		StreamResult result = new StreamResult(new File(htmlFileName));

		// set up transformation
		TransformerFactory tf = TransformerFactory.newInstance();
		javax.xml.transform.Transformer t = tf.newTransformer(
				new StreamSource(new File(xslFileName)));
		t.setOutputProperty(OutputKeys.INDENT, "yes");			
		
		// run transformation
		t.transform(new StreamSource(xmlFileName), result);
	}

	public static void saveToXML(Document document, String xmlFileName) 
			throws TransformerException {
		StreamResult result = new StreamResult(new File(xmlFileName));
		
		// set up transformation
		TransformerFactory tf = TransformerFactory.newInstance();
		javax.xml.transform.Transformer t = tf.newTransformer();
		t.setOutputProperty(OutputKeys.INDENT, "yes");			
		
		// run transformation
		t.transform(new DOMSource(document), result);
	}
}