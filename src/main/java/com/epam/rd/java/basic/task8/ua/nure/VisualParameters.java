package com.epam.rd.java.basic.task8.ua.nure;

import java.math.BigInteger;

//@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(name = "", propOrder = {
//        "stemColour",
//        "leafColour",
//        "aveLenFlower"
//})
public class VisualParameters {

//    @XmlElement(required = true)
    protected String stemColour;
//    @XmlElement(required = true)
    protected String leafColour;
    protected AveLenFlower aveLenFlower;

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String value) {
        this.stemColour = value;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String value) {
        this.leafColour = value;
    }


    public AveLenFlower getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(AveLenFlower value) {
        this.aveLenFlower = value;
    }

//    @XmlAccessorType(XmlAccessType.FIELD)
//    @XmlType(name = "", propOrder = {
//            "value"
//    })


}