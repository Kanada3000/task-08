package com.epam.rd.java.basic.task8.constants;

/**
 * Holds elements and attributes names from the XML schema.
 * @author D.Kolesnikov
 *
 */
public enum XML {
	// these are tags
	FLOWERS("flowers"), FLOWER("flower"), VISUAL_PARAMETERS("visualParameters"),
	AVE_LEN_FLOWER("aveLenFlower"), GROWING_TIPS("growingTips"), TEMPRETURE("tempreture"),
	LIGHTNING("lighting"), WATERING("watering"), NAME("name"), SOIL("soil"),
	ORIGIN("origin"), MULTIPLYING("multiplying"), STEM_COLOUR("stemColour"),
	LEAF_COLOUR("leafColour")

	// these are attributes
	,MEASURE("measure"), LIGHT_REQUIRING("lightRequiring");

	private String value;

	public String value() {
		return value;
	}

	XML(String value) {
		this.value = value.intern();
	}
}
