package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.XML;
import com.epam.rd.java.basic.task8.ua.nure.*;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.math.BigInteger;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;

	private String currentElement; // <-- current element name holder

	private Flowers flowers; // <-- main container
	private Flower flower;
	private AveLenFlower aveLenFlower;
	private GrowingTips growingTips;
	private Lighting lighting;
	private Tempreture tempreture;
	private VisualParameters visualParameters;
	private Watering watering;

	public Flowers getFlowers(){return flowers;}

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void parse() throws ParserConfigurationException, SAXException,
			IOException, XMLStreamException {

		XMLInputFactory factory = XMLInputFactory.newInstance();

		factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);

		XMLEventReader reader = factory.createXMLEventReader(
				new StreamSource(xmlFileName));

		while (reader.hasNext()) {
			XMLEvent event = reader.nextEvent();

			// skip any empty content
			if (event.isCharacters() && event.asCharacters().isWhiteSpace())
				continue;

			// handler for start tags
			if (event.isStartElement()) {
				if (isStart(event)) continue;
			}

			// handler for contents
			if (event.isCharacters()) {
				if (isCharacter(event)) continue;
			}

			// handler for end tags
			if (event.isEndElement()) {
				if (isEnd(event)) continue;
			}
		}
		reader.close();
	}

	private boolean isEnd(XMLEvent event) {
		EndElement endElement = event.asEndElement();
		String localName = endElement.getName().getLocalPart();

		if (localName == XML.FLOWER.value()) {
			flowers.getFlower().add(flower);
			return true;
		}

		if (localName == XML.AVE_LEN_FLOWER.value()) {
			visualParameters.setAveLenFlower(aveLenFlower);
			return true;
		}

		if (localName == XML.GROWING_TIPS.value()) {
			flower.setGrowingTips(growingTips);
		}

		if (localName == XML.LIGHTNING.value()) {
			growingTips.setLighting(lighting);
			return true;
		}

		if (localName == XML.TEMPRETURE.value()) {
			growingTips.setTempreture(tempreture);
			return true;
		}

		if (localName == XML.VISUAL_PARAMETERS.value()) {
			flower.setVisualParameters(visualParameters);
			return true;
		}

		if (localName == XML.WATERING.value()) {
			growingTips.setWatering(watering);
			return true;
		}
		return false;
	}

	private boolean isCharacter(XMLEvent event) {
		Characters characters = event.asCharacters();

		if (currentElement == XML.NAME.value()) {
			flower.setName(characters.getData());
			return true;
		}

		if (currentElement == XML.SOIL.value()) {
			flower.setSoil(characters.getData());
			return true;
		}

		if (currentElement == XML.ORIGIN.value()) {
			flower.setOrigin(characters.getData());
			return true;
		}

		if (currentElement == XML.STEM_COLOUR.value()) {
			visualParameters.setStemColour(characters.getData());
			return true;
		}

		if (currentElement == XML.LEAF_COLOUR.value()) {
			visualParameters.setLeafColour(characters.getData());
			return true;
		}

		if (currentElement == XML.AVE_LEN_FLOWER.value()) {
			aveLenFlower.setValue(BigInteger.valueOf(Long.parseLong(characters.getData())));
			return true;
		}

		if (currentElement == XML.TEMPRETURE.value()) {
			tempreture.setValue(BigInteger.valueOf(Long.parseLong(characters.getData())));
			return true;
		}

		if (currentElement == XML.WATERING.value()) {
			watering.setValue(BigInteger.valueOf(Long.parseLong(characters.getData())));
			return true;
		}

		if (currentElement == XML.MULTIPLYING.value()) {
			flower.setMultiplying(characters.getData());
			return true;
		}
		return false;
	}

	private boolean isStart(XMLEvent event) {
		StartElement startElement = event.asStartElement();
		currentElement = startElement.getName().getLocalPart();

		// WARNING!!!
		// here and below we use '==' operation to compare two INTERNED STRINGS
		if (currentElement == XML.FLOWERS.value()) {
			flowers = new Flowers();
			return true;
		}

		if (currentElement == XML.FLOWER.value()) {
			flower = new Flower();
			return true;
		}

		if (currentElement == XML.AVE_LEN_FLOWER.value()) {
			aveLenFlower = new AveLenFlower();
			Attribute attribute = startElement.getAttributeByName(
					new QName(XML.MEASURE.value()));
			if (attribute != null)
				aveLenFlower.setMeasure(attribute.getValue());
			return true;
		}

		if (currentElement == XML.GROWING_TIPS.value()) {
			growingTips = new GrowingTips();
			return true;
		}

		if (currentElement == XML.LIGHTNING.value()) {
			lighting = new Lighting();
			Attribute attribute = startElement.getAttributeByName(
					new QName(XML.LIGHT_REQUIRING.value()));
			if (attribute != null)
				lighting.setLightRequiring(attribute.getValue());
			return true;
		}

		if (currentElement == XML.TEMPRETURE.value()) {
			tempreture = new Tempreture();
			Attribute attribute = startElement.getAttributeByName(
					new QName(XML.MEASURE.value()));
			if (attribute != null)
				tempreture.setMeasure(attribute.getValue());
			return true;
		}

		if (currentElement == XML.VISUAL_PARAMETERS.value()) {
			visualParameters = new VisualParameters();
			return true;
		}

		if (currentElement == XML.WATERING.value()) {
			watering = new Watering();
			Attribute attribute = startElement.getAttributeByName(
					new QName(XML.MEASURE.value()));
			if (attribute != null)
				watering.setMeasure(attribute.getValue());
			return true;
		}
		return false;
	}

}